import React from 'react';
import {
   BrowserRouter as Router,
   Switch,
   Route
 } from "react-router-dom";
import './App.css';
import FoodPage from './pages/food/FoodPage';
import {configs} from "./pages/food/configs"
 
 
function App() {
   return (
   <div className = "App" >
       <Router>
           <Switch>
               <Route exact path="/">
                   <FoodPage configs={configs}></FoodPage>
               </Route>
           </Switch>
       </Router>
   </div>
   );
}
 
export default App;
