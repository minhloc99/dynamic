import React from 'react';
import {mapper} from "./mapper";

export default function ComponentPicker(props) {
    // Correct! JSX type can be a capitalized variable.
    const DynamicComponent = mapper[props.config.type];
    
    return <DynamicComponent {...props} />;
}