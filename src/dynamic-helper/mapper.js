import AppFooter from '../modules/views/AppFooter';
import AppAppBar from "../modules/views/AppAppBar"
import { Link } from 'react-router-dom';
import Banner from '../modules/views/Banner';
import Essay from "../modules/views/Essay"
import AppBody from '../modules/views/AppBody';
import Album from '../modules/views/Album';

export const mapper = {
    AppAppBar:AppAppBar,
    AppFooter:AppFooter,
    AppBody:AppBody,
    Banner:Banner,
    Essay:Essay,
    Link:Link,
    Album: Album
}