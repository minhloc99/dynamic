import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    width:"70%",
    height:"auto",
    margin:"auto"
  },
  image:{
    width:"100%",
    margin:"auto"
  }
}));

export default function Banner(props) {
  const classes = useStyles();
  var src=require("../../assets/img/"+props.config.data.imageUrl);

  return (
    <div className={classes.root}>
      <img className={classes.image} src={src} alt=""/>
    </div>
  );
}