import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    boxSizing:"border-box",
    alignItems:"center",
    display:"flex",
    height:"100px"
  },
  navBar: {
    display: "block",
    margin: "auto"
  },
  list: {
    display:"flex",
    listStyle:"none",
    paddingLeft: 0
  },
  listItem: {
    display: "list-item",
    textAlign: "-webkit-match-parent",
    textDecoration: "none",
    padding:"10px"
  }
}));

export default function AppFooter(props) {
  const classes = useStyles();

  return (
    <footer className={classes.root}>
      <nav className={classes.navBar}>
        <ul className={classes.list}>
          {props.config.menu.map((configItem, index)=>{
            return <li className={classes.listItem}>
                     <Link href={configItem.data.href}>
                       {configItem.data.content}
                      </Link>
                  </li>
          })}
        </ul>
      </nav>
    </footer>
  );
}
