import React from "react";
import ComponentPicker from "../../dynamic-helper/ComponentPicker";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    margin:"auto",
    width: "100%"
  }
});

 export default function AppBody(props) {
  const classes = useStyles();

    return (
      <React.Fragment>
        <main className={classes.root}>
            {
                props.config.sections.map((configItem, index)=>{
                    return <ComponentPicker key={index} config={configItem}></ComponentPicker>
                })
            }
        </main>
      </React.Fragment>
    );
  }