import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: "70%",
    height: "auto"
  },
}));

export default function Album(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <GridList cellHeight={160} className={classes.gridList} cols={2}>
        {props.config.images.map((imageConfig) => {
            var src=require("../../assets/img/"+imageConfig.data.imageUrl);

          return <GridListTile key={src} cols={imageConfig.cols || 1}>
            <img src={src} alt={imageConfig.title} />
          </GridListTile>
        })}
      </GridList>
    </div>
  );
}