import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from "../components/Typography";

const useStyles = makeStyles({
  root: {
    margin:"auto",
    width: "70%"
  },
  title:{
    marginTop:"10px",
    borderBottom: "solid 1px green"
  },
  content: {
    marginTop:"10px"
  },
});

export default function Essay(props) {
  const classes = useStyles();

  return (
     <section className={classes.root}>
       <Typography className={classes.title} gutterBottom variant="h5" component="h2">
          {props.config.data.title}
        </Typography>
        <Typography className={classes.content} variant="body2" color="textSecondary" component="p">
          {props.config.data.htmlContent}
        </Typography>
     </section>
  );
}
