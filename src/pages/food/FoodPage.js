import withRoot from '../../modules/withRoot';
import React from 'react';
import ComponentPicker from "../../dynamic-helper/ComponentPicker";

function Index(props) {
  return (
    <React.Fragment>
      {
        props.configs.map((config, index)=>{
          return <ComponentPicker key={index} config={config}/>
        })
      }
    </React.Fragment>
  );
}

export default withRoot(Index);
