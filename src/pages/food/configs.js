
export const configs = [
    {
        id: 1,
        order : 1,
        type: "AppAppBar",
        data:{
            title:"クアン麺の作り方"
        }
    },
    {
        id: 2,
        order : 2,
        type: "AppBody",
        data:{
            htmlContent:""
        },
        sections: [
            {
                id:1,
                order: 1,
                type: "Banner",
                data:{
                    title:"",
                    content:"クアン麺の作り方",
                    imageUrl: 'miquang.jpg'
                }
            },
            {
                id:2,
                order: 2,
                type: "Essay",
                data:{
                    title:"",
                    htmlContent:"クアン麺はダナンの伝統的な料理です。 クアン麺はベトナム中で有名になりました。 クアン麺には、野菜、肉、ピーナッツ、ソース、麺が含まれます。 今日はクアン麺の作り方を学びましょう。",
                    imageUrl:""
                }
            },
            {
                id:3,
                order: 3,
                type: "Essay",
                data:{
                    title:"ご飯を浸して洗う",
                    htmlContent:"米をよく洗い、汚れを落とします。 その後、米を約8時間浸します。 漬け込んだご飯は柔らかくなります。 ご飯が柔らかくなればなるほど、加工がしやすくなります。",
                    imageUrl:""
                }
            },
            {
                id:4,
                order: 4,
                type: "Essay",
                data:{
                    title:"米を小麦粉に挽く",
                    htmlContent:"ご飯を漬けたら、お米を取り出します。 米をすり鉢に入れて挽く。 米を挽きながら水も加えます。 昔は、精米所で精米していた。 精米は手動で行われます。 そのため、完了するまでに時間がかかります。 現在、精米はほとんど機械で行われています。",
                    imageUrl:""
                }
            },
            {
                id:5,
                order: 5,
                type: "Essay",
                data:{
                    title:"米粉を整形して蒸し",
                    htmlContent:"オートクレーブは粘土で作られた窯です。 ケーキを蒸すための燃料は、通常、乾燥した木材またはもみがらです。 炉の真ん中には大きな鍋があります。 鍋に50％の水を注いだ。 鍋の上は大きな布で覆われています。ケーキの形は次のとおりです。米粉溶液を布の表面に均一に広げます。 表面が滑らかな道具を使います。 このツールは通常、アルミニウムまたはココナッツの殻から作られています。 生地の表面に米粉を敷いた後、鍋で蓋をします。 鍋の水が沸騰すると、蒸気が上昇して布から脱出します。 蒸気の熱下で、3〜5分以内に、米粉は餅に調理されます。 お餅はとても柔らかくて薄いです。",
                    imageUrl:""
                }
            },
            {
                id:6,
                order: 6,
                type: "Essay",
                data:{
                    title:"麺をに切る",
                    htmlContent:"オートクレーブは粘土で作られた窯です。細い竹の棒を使って、ケーキを布の表面から引き離します。ケーキの表面を食用油の薄い層でこすります。 その後、ナイフまたは機械で切断できます。",
                    imageUrl:""
                }
            },
            {
                id:7,
                order: 7,
                type: "Album",
                images:[
                    {
                        id:1,
                        order: 1,
                        data:{
                           title:"",
                           imageUrl:"gomi.jpg",
                           cols:2
                        }
                    },
                    {
                        id:2,
                        order: 1,
                        data:{
                            title:"",
                            imageUrl:"lo.jpg",
                            cols:2
                        }
                    },
                    {
                        id:3,
                        order: 1,
                        data:{
                            title:"",
                            imageUrl:"maycatmi.jpg",
                            cols:2
                        }
                    },
                    {
                        id:4,
                        order: 1,
                        data:{
                            title:"",
                            imageUrl:"ngamgao.jpg",
                            cols:2
                        }
                    },
                    {
                        id:5,
                        order: 1,
                        data:{
                            title:"",
                            imageUrl:"thai_mi.jpg",
                            cols:2
                        }
                    },
                    {
                        id:6,
                        order: 1,
                        data:{
                            title:"",
                            imageUrl:"vogao.jpeg",
                            cols:2
                        }
                    }
                ]

            }
        ]
    },
    {
        id: 3,
        order : 3,
        type: "AppFooter",
        data:{
            title:"",
            content:"",
            imageUrl:""
        },
        menu: [
            {
                id: 1,
                order: 1,
                type: "Link",
                data:{
                    title:"",
                    content:"現地",
                    href:"/place"
                }
            },
            {
                id: 2,
                order: 2,
                type: "Link",
                data:{
                    title:"",
                    content:"食べ物",
                    href:"/food"
                }
            },
            {
                id: 3,
                order: 3,
                type: "Link",
                data:{
                    title:" ",
                    content:"お問い合わせ",
                    href:"/contact"
                }
            }
        ]
    }
];